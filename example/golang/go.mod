module voicelab.ai/example

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	google.golang.org/grpc v1.22.0
)
