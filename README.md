### Application "example" reads audio samples from standard input, sends them to ASR using gRPC and receives recognition.

&nbsp;

#### 1. Build application:

```sh
    export GO111MODULE=on
    go build
```

#### 2. Simple usage is:

```sh
    cat ../data/sample16kHz.wav | \
            ./example -addr demo.voicelab.ai:7722 \
            -pid PID -pass PASSWORD \
            -conf_name 16000_pl_PL
```

&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; **where `PID` and `PASSWORD` are access parameters.**

&nbsp;

#### 3. Use option `-c arg` to pass protocol parameters with ***metadata_config.json.*** Parameters represent straight forward protocol metadata variables. Values in ***json*** file are substituted by application arguments if specified (with exception of ***"contenttype"*** - it is always set by the application). 



```sh
    cat ../data/sample16kHz.wav | \
            ./example -addr demo.voicelab.ai:7722 \
            -c metadata_config.json \
            -conf_name 16000_pl_PL
```

#### 4. Use `-verbose` to follow all updates:


```sh
    cat ../data/sample16kHz.wav | \
            ./example -addr demo.voicelab.ai:7722 \
            -c metadata_config.json \
            -conf_name 16000_pl_PL \
    	-verbose
```

#### 5. Use other audio source. To use simple microphone input:


```sh
    arecord -r 16000 -f S16_LE | \
            ./example -addr demo.voicelab.ai:7722 \
            -c metadata_config.json \
            -conf_name 16000_pl_PL \
            -speech_complete_timeout=2s
```

#### Or to recognize for given period of time:

```sh
    arecord -r 8000 -f S16_LE | \
            ./example -addr demo.voicelab.ai:7722 \
            -c metadata_config.json \
            -conf_name 8000_pl_PL & \
    	sleep 5 && killall arecord
```
